# Задача 3. Наименьший делитель

def smallest_divisor(number):
    for divisor in range(2, number//2 + 1):
        if number % divisor == 0:
            return divisor
    return number

number = int(input("Введите число: "))

smallest_divisor = smallest_divisor(number)
print(f"Наименьший делитель, больше единицы: {smallest_divisor}")
