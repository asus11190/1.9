def sum_of_digits(number):
    digit_sum = 0
    while number > 0:
        digit_sum += number % 10
        number //= 10
    return digit_sum

def count_digits(number):
    digit_count = 0
    while number > 0:
        digit_count += 1
        number //= 10
    return digit_count

number = int(input('Введите число: '))

digit_sum = sum_of_digits(number)
digit_count = count_digits(number)
difference = digit_sum - digit_count

print("Сумма чисел:", digit_sum)
print("Количество цифр в числе:", digit_count)
print("Разность суммы и количества цифр:", difference)

#aded home test branch